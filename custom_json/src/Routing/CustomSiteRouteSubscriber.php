<?php

namespace Drupal\custom_json\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Custom Site router subscriber for extended Site information form.
 */
class CustomSiteRouteSubscriber extends RouteSubscriberBase {

    /**
     * {@inheritdoc}
     */
    protected function alterRoutes(RouteCollection $collection) {
        if($route = $collection->get('system.site_information_settings')) {
            $route->setDefault('_form', 'Drupal\custom_json\Form\SiteApiKeyForm');
        }
    }
}
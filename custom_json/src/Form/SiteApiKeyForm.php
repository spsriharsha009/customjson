<?php

namespace Drupal\custom_json\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\system\Form\SiteInformationForm;

/**
 * Configure the Site API key settings for this site.
 */
class SiteApiKeyForm extends SiteInformationForm {

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $site_config = $this->config('system.site');
        $form = parent::buildForm($form, $form_state);

        $form['site_information']['site_api_key'] = [
          '#type' => 'textfield',
          '#title' => t('Site API Key'),
          '#default_value' => $site_config->get('site_api_key'),
          '#description' => $this->t('The Site API Key of the site'),
        ];

        if(!empty($site_config->get('site_api_key'))) {
            $form['actions']['submit']['#value'] = $this->t('Update Configuration');
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $site_api_key = $form_state->getValue('site_api_key');
        $this->config('system.site')
             ->set('site_api_key', $site_api_key)
             ->save();
        
        parent::submitForm($form, $form_state);
        $this->messenger()->addMessage($this->t('The configuration  have been savd with Site API key %site_api_key', ['%site_api_key' => $site_api_key]));        
    }
}

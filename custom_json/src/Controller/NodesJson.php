<?php

namespace Drupal\custom_json\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for NodesJson.
 */
class NodesJson extends ControllerBase {

    /**
     * Return the json Output of the data.
     * 
     * @param $site_api_key
     *  Site API Key.
     * 
     * @param $id
     *  Node id.
     * 
     * @return Symfony\Component\HttpFoundation\JsonResponse
     *   Return the JSON response.
     */
    public function content ($site_api_key, $id) {
        $config = $this->config('system.site');
        $site_key = $config->get('site_api_key');
        $node = Node::load($id);
        if ($site_api_key != $site_key) {
            return FALSE;
        }
        $response['data'] = $node;
        $response['method'] = 'GET';
        
        return new JsonResponse( $response );
    }

    /**
     * check if the user has access to the node json service.
     * 
     * @param $site_api_key
     *   Site API key
     * 
     * @param $id
     *   Node id
     * 
     * @return \Drupal\Core\Access\AccessResult
     */
    public function access($site_api_key, $id) {
        $config = $this->config('system.site');
        $site_key = $config->get('site_api_key');

        return AccessResult::allowedIf($site_api_key == $site_key);
    }
}